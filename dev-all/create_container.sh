#!/bin/bash

cd `dirname "$0"`

source conf.sh
source ${RIPPLE_SRC_HOME}/conf.sh

# remove container if exists
docker stop ${NAME}
docker rm ${NAME}

echo "use 'docker attach ${NAME}' to re-attach"
echo "use 'docker start ${NAME}' to re-start"
echo "use 'docker stop ${NAME}' to stop"

docker run -P -p ${BACKEND_PORT:-8000}:8000/tcp -p  ${FRONTEND_PORT:-3000}:3000/tcp -p ${SUPERVISORD_PORT:-9001}:9001/tcp -it -d --name ${NAME} \
    --mount type=bind,source=${RIPPLE_SRC_HOME},target=/home/src \
    --mount type=bind,source=${RIPPLE_REACT},target=/home/ripple-react \
    --mount type=bind,source=${RIPPLE_DJANGO},target=/home/ripple-django \
    ${IMAGE_NAMESPACE}/ripple_dev bash

# initialise the container (this creates various links to map into the source tree
docker exec ${NAME} init-container.sh

if [[ "$1" == "dontrun" ]]; then
    echo "You need to start the server manually running runserver.sh in the container"
else
    # starts the dev server processes
    echo "starting frontend and backend"
    docker exec ${NAME} runserver.sh
fi