#! /bin/bash

ln -s  /home/node_modules/.bin /home/src/node_modules/.bin
ln -s -r /home/node_modules/* /home/src/node_modules
mkdir /home/src/node_modules/ripple-react
ln -s /home/ripple-react/dist /home/src/node_modules/ripple-react/dist
ln -s /home/ripple-react/package.json /home/src/node_modules/ripple-react/package.json