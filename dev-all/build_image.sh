#!/bin/bash

cd `dirname "$0"`

source conf.sh
source ${RIPPLE_SRC_HOME}/conf.sh

cp ${RIPPLE_SRC_HOME}/package.json .
cp ${RIPPLE_SRC_HOME}/requirements.txt .

docker build "$@" -t ${IMAGE_NAMESPACE}/ripple_dev .