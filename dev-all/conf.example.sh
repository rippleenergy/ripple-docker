#!/bin/bash

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
# copy to conf.sh and update according to your local setup
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# your docker account if you want to push into the cloud
IMAGE_NAMESPACE=yourname

# absolute path to your ripple-web project source
RIPPLE_SRC_HOME=/path/to/ripple/web/project
